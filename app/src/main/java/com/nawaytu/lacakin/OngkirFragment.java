package com.nawaytu.lacakin;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;

public class OngkirFragment extends Fragment {

    /*

    String[] logistikList = new String[] {"JNE", "TIKI", "WAHANA", "POS", "J&T"};
    int[] images = new int[]{R.drawable.jne,
            R.drawable.tiki,

            R.drawable.wahana,
            R.drawable.pos,
            R.drawable.jt};

    ArrayList<HashMap<String, String>> data = new ArrayList<HashMap<String, String>>();
    SimpleAdapter adapter;
    */

    public static final String TITLE = "Ongkir";

    public static OngkirFragment newInstance() {
        return new OngkirFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        /*
        HashMap<String, String> map = new HashMap<String, String>();
        for (int i = 0; i < logistikList.length; i++) {
            map = new HashMap<String, String>();
            map.put("Logistik", logistikList[i]);
            map.put("Image", Integer.toString(images[i]));
            data.add(map);
        }

        String[] from = {"Logistik","Image"};
        int[] to = {R.id.namaLogistik, R.id.logoLogistik};

        adapter = new SimpleAdapter(getActivity(), data, R.layout.custom_layout, from, to);
        setListAdapter(adapter);
        return super.onCreateView(inflater, container, savedInstanceState);
        */

        View view = inflater.inflate(R.layout.fragment_ongkir, container, false);

//        String[] logistikList = new String[] {"JNE", "TIKI", "WAHANA", "POS", "J&T"};

        List<String> logistikList = new ArrayList<>();
        logistikList.add("JNE");
        logistikList.add("TIKI");
        logistikList.add("WAHANA");
        logistikList.add("POS");
        logistikList.add("J&T");

        String apiKey = Configuration.API_KEY;


        //ListView listView = (ListView) view.findViewById(R.id.logistikItem);

        ArrayAdapter<String> listViewAdapter = new ArrayAdapter<String>(
                getActivity(),
                android.R.layout.simple_list_item_1,
                logistikList
        );

       // listView.setAdapter(listViewAdapter);

        return view;
    }

    /*
    public void onStart() {
        super.onStart();
        getListView().setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                //Toast.makeText(getActivity(), data.get(pos).get("logistikList"), Toast.LENGTH_SHORT).show();
            }
        });
    }
    */
}
package com.nawaytu.lacakin;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

public class ViewPagerAdapter extends FragmentStatePagerAdapter {

    private static int TAB_COUNT = 3;

    public ViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                return ResiFragment.newInstance();
            case 1:
                return OngkirFragment.newInstance();
            case 2:
                return AboutFragment.newInstance();
        }
        return null;
    }

    @Override
    public int getCount() {
        return TAB_COUNT;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return ResiFragment.TITLE;

            case 1:
                return OngkirFragment.TITLE;

            case 2:
                return AboutFragment.TITLE;
        }
        return super.getPageTitle(position);
    }
}
